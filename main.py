from flask import Flask, request, jsonify, render_template

app = Flask(__name__)

@app.route('/')
def index():
    return render_template("index.html")

@app.route('/api/v1/brands', methods=['GET'])
def get_brands():
    with open("resources/brands.txt", "r") as f:
        brand_list = f.read().splitlines()
    return jsonify(brand_list)

@app.route('/api/v1/groups', methods=['GET'])
def get_groups():
    return jsonify(group_list)

group_list = list()
@app.route('/api/v1/groups', methods=['POST'])
def post_group():
    group_list.append(request.json['group'])
    return jsonify(group_list), 201

if __name__ == '__main__':
    app.run()