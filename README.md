# Kaiyo

Coding Exercise
>This exercise is a simple Flask web application to select a group of brands, list the results, and persist it in memory.

Instructions to Run
```shell
export FLASK_APP=main.py
flask run
```

Access Website Locally
>http://127.0.0.1:5000

Testing API Endpoints
- Get List of Brands
    - `curl http://127.0.0.1:5000/api/v1/brands`
- Get List of Groups
    - `curl http://127.0.0.1:5000/api/v1/groups`
- Post Group
    - `curl -i -H "Content-Type: application/json" -X POST -d '{"group":"Test"}' http://127.0.0.1:5000/api/v1/groups`
